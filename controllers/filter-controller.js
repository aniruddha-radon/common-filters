app.controller("filterController", function($scope) {
    $scope.date = new Date();
    
    $scope.points = [{
        text: "2. We put the ng-app directive on the html or body tag with the name of the app. e.g: ng-app=\"todoApp\" ",
        index: 1
    }, {
        text: "3. The ng-controller directive is used to tell AngularJS which controller is going to govern this section",
        index: 2
    }, {
        text: "1. We get(install) the AngularJS into our project by importing the CDN into our headers script tag with the src set to 'https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.js'",
        index: 0
    }]
})